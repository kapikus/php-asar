<?php

/*
  .asar internals

  The entire structure is a piece of cake. .asar has the following internal format.

  | UInt32: header_size | String: header | Bytes: file1 | ... | Bytes: file42 |

  header_size is an 8-bit binary data segment that stores the size of the header segment.
  The header segment stores the directory tree of all stored files in JSON format.
  After that, it’s all about the content of the concatenated files like the .tar archive format.
 */

class ASAR
{
	const ERR_OPENING_ARCHIVE   = 'err opening archive';
	const ERR_ARCHIVE_MALFORMED = 'err archive malformed';
	const ERR_ARCHIVE_NOT_OPEN  = 'err archive not open';
	const ERR_FILE_NOT_FOUND    = 'err file not found';

	private $file_name;
	private $file_handle;
	private $save_handle;
	private $header;
	private $base_offset;
	private $files_add = [];
	private $files_del = [];
	private $error;

	public function __construct(?string $fileName = '')
	{
		if ($fileName) {
			$this->open($fileName);
		}
	}

	public function __destruct()
	{
		if ($this->file_handle) {
			fclose($this->file_handle);
		}
	}

	public function open(string $fileName) : bool
	{
		$this->header    = null;
		$this->files_add = [];
		$this->files_del = [];
		$this->error = null;

		if ($this->file_handle) {
			fclose($this->file_handle);
		}

		if (!$handle = fopen($fileName, 'rb')) {
			return $this->error(self::ERR_OPENING_ARCHIVE);
		}
		$this->file_handle = $handle;
		$this->file_name = $fileName;

		return $this->decode();
	}

	public function save(string $fileName = null) : bool
	{
		$this->error = null;
		if (!$this->header) {
			return $this->error(self::ERR_ARCHIVE_NOT_OPEN);
		}

		if (!$fileName) {
			$fileName = pathinfo($this->file_name, PATHINFO_FILENAME) . '.saved.asar';
		}

		$tmpFileName = tempnam(sys_get_temp_dir(), 'asar');
		$this->save_handle = fopen($tmpFileName, 'w+b');

		$header = $this->updateHeader($this->header);
		$hrString = json_encode($header, JSON_UNESCAPED_SLASHES);
		$hrStringSize = strlen($hrString);
		fwrite($this->save_handle, pack('V*', 4, $hrStringSize + 8, $hrStringSize + 4, $hrStringSize));
		fwrite($this->save_handle, $hrString);

		$this->saveFiles($this->header);

		rename($tmpFileName, $fileName);
		fclose($this->save_handle);

		$this->open($fileName);

		return true;
	}

	private function saveFiles(array $root, string $path = '')
	{
		foreach ($root['files'] as $entityName => $entity) {
			$p = $path . ($path ? '/' : '') . $entityName;
			if (key_exists('files', $entity)) {
				$this->saveFiles($entity, $p);
			} else {
				$content = $this->getFile($p);
				if ($content) {
					fwrite($this->save_handle, $content);
				}
			}
		}
	}

	private function updateHeader(array $root, int &$offset = 0, string $path = '') : array
	{
		$header = ['files' => []];

		// TODO: new files & folders
		foreach ($root['files'] as $entityName => $entity) {
			$p = $path . ($path ? '/' : '') . $entityName;
			if (in_array($p, $this->files_del)) {
				continue;
			}
			if (isset($entity['size'])) {
				if (isset($this->files_add[$p])) {
					$size = strlen($this->files_add[$p]);
					$header['files'][$entityName] = [
						'size'   => $size,
						'offset' => (string)$offset
					];
					$offset += $size;
				} else {
					$header['files'][$entityName] = $entity;
					if (!isset($entity['unpacked'])) {
						$header['files'][$entityName]['offset'] = (string)$offset;
						$offset += $entity['size'];
					}
				}
			} else {
				$header['files'][$entityName] = [];
				if (isset($entity['unpacked'])) {
					$header['files'][$entityName]['unpacked'] = $entity['unpacked'];
				}
				if (isset($entity['link'])) {
					$header['files'][$entityName]['link'] = $entity['link'];
				}
				if (key_exists('files', $entity)) {
					$header['files'][$entityName] += $this->updateHeader($entity, $offset, $p);
				}
			}
		}

		return $header;
	}

	private function decode() : bool
	{
		$this->error = null;

		fseek($this->file_handle, 0);

		if (!$buff = fread($this->file_handle, 16)) {
			return $this->error(self::ERR_ARCHIVE_MALFORMED);
		}

		$hrInfo = unpack('Vdata_size/Vhr_size/Vhr_object_size/Vhr_string_size', $buff);
		if (
			$hrInfo['data_size'] != 4 || $hrInfo['hr_size'] - 4 != $hrInfo['hr_object_size'] ||
			!$buff = fread($this->file_handle, $hrInfo['hr_string_size'])
		) {
			return $this->error(self::ERR_ARCHIVE_MALFORMED);
		}

		$header = json_decode($buff, true);

		if (json_last_error() || !is_array($header)) {
			return $this->error(self::ERR_ARCHIVE_MALFORMED);
		}

		$this->header = $header;
		$this->base_offset = $hrInfo['hr_size'] + 8;

		return true;
	}

	public function getFile(string $filePath) : mixed
	{
		$this->error = null;

		if (!$this->header) {
			return $this->error(self::ERR_ARCHIVE_NOT_OPEN);
		}

		if (isset($this->files_del[$filePath])) {
			return $this->error(self::ERR_FILE_NOT_FOUND);
		}

		if (isset($this->files_add[$filePath])) {
			return $this->files_add[$filePath];
		}

		$currentPath = $this->header;
		foreach (explode('/', $filePath) as $entity) {
			if (isset($currentPath['files'][$entity])) {
				$currentPath = $currentPath['files'][$entity];
				if (isset($currentPath['size'])) {
					if (isset($currentPath['unpacked']) || $currentPath['size'] == 0) {
						return '';
					}
					break;
				}
			} else {
				return $this->error(self::ERR_FILE_NOT_FOUND);
			}
		}

		if (!isset($currentPath['size'])) {
			return $this->error(self::ERR_FILE_NOT_FOUND);
		}

		fseek($this->file_handle, $currentPath['offset'] + $this->base_offset);
		if (($buff = fread($this->file_handle, $currentPath['size'])) === false) {
			return $this->error(self::ERR_ARCHIVE_MALFORMED);
		}

		return $buff;
	}

	public function setFile(string $filePath, ?string $content = '') : bool
	{
		$this->error = null;

		if (!$this->header) {
			return $this->error(self::ERR_ARCHIVE_NOT_OPEN);
		}

		$this->files_add[$filePath] = $content;
		if (in_array($filePath, $this->files_del)) {
			unset($this->files_del[$filePath]);
		}

		return true;
	}

	public function deleteFile(string $filePath) : bool
	{
		$this->error = null;

		if (!$this->header) {
			return $this->error(self::ERR_ARCHIVE_NOT_OPEN);
		}

		$this->files_del[] = $filePath;
		if (key_exists($filePath, $this->files_add)) {
			unset($this->files_add[$filePath]);
		}

		return true;
	}

	private function error(?string $err = '') : bool
	{
		$this->error = $err;
		return false;
	}

	public function getError() : ?string
	{
		return $this->error;
	}
}
